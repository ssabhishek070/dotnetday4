﻿using Enum.constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enum.repository
{
    internal class UserRepository :IBillable,IOrderRepository
    {
        public void GenerateInVoice(InvoiceOptions invoiceOptions)
        {
            switch (invoiceOptions)
            {
                case InvoiceOptions.email:
                    Console.WriteLine("invoice emailed");
                    break;
                case InvoiceOptions.pdf:
                    Console.WriteLine("invoice pdf send");
                    break;
                case InvoiceOptions.sms:
                    Console.WriteLine("invoice sms send");
                    break;
                case InvoiceOptions.print:
                    Console.WriteLine("invoice printed");
                    break;
                default:
                    Console.WriteLine("invalid option");
                    break;

            }

        }

    }
}
