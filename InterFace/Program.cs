﻿// See https://aka.ms/new-console-template for more information
using InterFace.model;
using InterFace.repository;

Console.WriteLine("Hello, World!");

ProductRepository prdctRepo = new ProductRepository();
Product prd = new Product() { Name="Mobile",Price=100000 };
Console.WriteLine(prdctRepo.addProduct(prd));

prd = new Product() { Name = "TV", Price = 1000 };
Console.WriteLine(prdctRepo.addProduct(prd));
prd = new Product() { Name = "PC", Price = 100 };
Console.WriteLine(prdctRepo.addProduct(prd));
prd = new Product() { Name = "Laptop", Price = 10000 };
Console.WriteLine(prdctRepo.addProduct(prd));

prdctRepo.printProduct();
Console.WriteLine("Enter the name of the product you want to delete");
string name = Console.ReadLine();
Console.WriteLine(prdctRepo.deleteProduct(name));
prdctRepo.printProduct();
//prdctRepo.addProduct
