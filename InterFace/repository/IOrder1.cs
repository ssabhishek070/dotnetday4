﻿using InterFace.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterFace.repository
{
    internal interface IOrder1
    {
        List<Product> BuyOrder();
        string CancelOrder();
    }
}
