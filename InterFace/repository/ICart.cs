﻿using InterFace.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterFace.repository
{
    internal interface ICart
    {
        string addProduct(Product p);
    }
}
