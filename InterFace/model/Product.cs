﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterFace.model
{
    internal class Product
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
