﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Regexpasss
{
    internal class Class1reg
    {
        public  string pattern = @"^[a-zA-Z0-9!@#$%^&*()]{8,}$";

        public  bool validatePassword(string Password)
        {
            if (pattern != null)
            {
                return Regex.IsMatch(Password, pattern);
            }
            else
            {
                return false;
            }
        }
        public string pattern2 = @"^[a-zA-Z0-9]+@[a-z]+[.][a-z]{2,3}$";
        public bool validateEmail(string email)
        {
            if (pattern2 != null)
            {
                return Regex.IsMatch(email, pattern2);
            }
            else
            {
                return false;
            }

        }


    }
}
