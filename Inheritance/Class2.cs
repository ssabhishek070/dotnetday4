﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance 
{
    internal class Class2 : Class1
    {
        public int Id { get; set; }
        public Class2(string firstName, string lastName, int id) : base(firstName, lastName)
        {
            Id = id;
        }
        public void fullname()
        {
            Console.WriteLine($"FullName is :: {FirstName} {LastName}");
        }
    }
}
